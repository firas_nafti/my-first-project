from flask import Flask, render_template, request, session, logging,url_for,redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine


app = Flask(__name__)

ENV = 'prod'

if ENV == 'dev':
    app.debug = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:Intermilan@localhost/lexus'
else:
    app.debug = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://nwxnioehcsdihm:d8de7e4748f51893f3c8ede5c0ce6a926d84671a7aca7429fe7c6b48009d77c7@ec2-34-236-215-156.compute-1.amazonaws.com:5432/d1otd5sbfvlie5'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
class User(db.Model):
    """User model"""
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    mail = db.Column(db.String(25), unique=True, nullable =False)
    password = db.Column(db.String(), nullable= False)

    def __init__(self, mail,password):
        self.mail = mail
        self.password = password



class Feedback(db.Model):
    __tablename__ = 'feedback'
    id = db.Column(db.Integer, primary_key=True)
    customer = db.Column(db.String(200), unique=True)
    dealer = db.Column(db.String(200))
    rating = db.Column(db.Integer)
    comments = db.Column(db.Text())

    def __init__(self, customer, dealer, rating, comments):
        self.customer = customer
        self.dealer = dealer
        self.rating = rating
        self.comments = comments


@app.route('/success', methods=['POST'])
def sign():
    if request.method == 'POST':
        mail = request.form['Email']
        password = request.form['Password']
        #print(mail, password)
        if mail == '' or password == '':
            return render_template('registration.html', message='Please enter required fields')
        if db.session.query(User).filter(User.mail == mail).count()==0:
            data = User(mail, password)
            db.session.add(data)
            db.session.commit()
            return render_template('index.html')
        return render_template('registration.html', message='This mail is already taken!')

@app.route('/home', methods= ['POST'])
def ent():
    if request.method == 'POST':
        email = request.form['mail']
        password = request.form['password']
        m = User.query.filter(User.mail == email).first()
        print(email,password)
        if m is None:
            return render_template('log.html', message='wrong email!')
        if m.password != password:
            return render_template('log.html', message='wrong password')
        if m.password == password:
            return render_template('index.html')
       # if db.session.query(User).filter(User.mail == mail).count()==1 and db.session.query(User).filter(User.password == password).count()>0 and User.mail == mail:
        #    return render_template('log.html')
        return render_template('log.html')

@app.route('/home1', methods= ['GET'])
def gethome():
    return render_template('home.html')

@app.route('/home2', methods= ['GET'])
def regis_now():
    return render_template('home.html')
@app.route('/about_us', methods= ['GET'])
def about():
    return render_template('about_us.html')
@app.route('/regist', methods= ['GET'])
def about2():
    return render_template('registration.html')

@app.route('/login')
def log():
    return render_template('log.html')  
@app.route('/')
def index():
    return render_template('home.html')

  
@app.route('/register')
def regist():
    return render_template('registration.html')  

@app.route('/login2')
def login2():
    return render_template('log.html')  

@app.route('/regist2')
def regist_2():
    return render_template('registration.html')  

@app.route('/home3')
def home_3():
    return render_template('home.html')  


@app.route('/home4')
def home_4():
    return render_template('home.html')  


@app.route('/map')
def map():
    return render_template('map.html') 

@app.route('/back_home')
def back_home3():
    return render_template('home.html') 

@app.route('/back')
def back_to_form():
    return render_template('index.html') 

@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        customer = request.form['customer']
        dealer = request.form['dealer']
        rating = request.form['rating']
        comments = request.form['comments']
        print(customer, dealer, rating, comments)
        if customer == '' or dealer == '':
            return render_template('index.html', message='Please enter required fields')
        if db.session.query(Feedback).filter(Feedback.customer == customer).count() == 0:
            data = Feedback(customer, dealer, rating, comments)
            db.session.add(data)
            db.session.commit()
            send_mail(customer, dealer, rating, comments)
            return render_template('success.html')
        return render_template('index.html', message='You have already submitted feedback')
@app.route('/regitration', methods=['GET'])
def regis():
    if request.method == 'GET':
        return render_template('registration.html')
        
if __name__ == '__main__':
    app.run()
